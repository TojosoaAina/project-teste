import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Country } from '../models/country.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
  getData() {
    return this.http.get<Country[]>(environment.URL_SYMFONY +  '/projects');
  }

  getOneData(id: number) {
    return this.http.get<Country[]>(environment.URL_SYMFONY +  '/project/' + id);
  }
  postExcel(formData: any) {
    return this.http.post(environment.URL_SYMFONY +  '/upload-excel', formData);
  }
}
