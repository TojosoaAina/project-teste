import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  constructor(private dataService: DataService, private router: Router) {

  }
  uploadFile(event: any) {
    let fileList: FileList = event.target.files;

    if (fileList.length < 1) {
      return;
    }

    let file: File = fileList[0];

    let formData:FormData = new FormData();
    formData.append('file', file, file.name);
    this.dataService.postExcel(formData)
    .subscribe({
      next: (data) => {

        this.router.navigate(['/dashboard']);
        location.reload();
      },
      error: (e) => console.error(e)
    });
  }
}
