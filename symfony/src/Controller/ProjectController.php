<?php

namespace App\Controller;

use App\Entity\Country;
use App\Entity\Music;
use App\Entity\Origin;
use App\Entity\Project;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api', name: 'api_')]
class ProjectController extends AbstractController
{
    #[Route('/projects', name: 'project_index', methods: ['get'])]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $groups = $doctrine
            ->getRepository(Project::class)
            ->findAll();

        $data = [];
        foreach ($groups as $group) {
            $music = $doctrine->getRepository(Music::class)->find($group->getId());

            $data[] = [
                'id' => $group->getId(),
                'group_name' => $group->getGroupName(),
                'origin' => $group->getOrigin()->getName(),
                'country' => $group->getCountry()->getName(),
                'start_year' => $group->getStartYear(),
                'separation_year' => $group->getSeparationYear(),
                'founders' => $group->getFounders(),
                'members' => $group->getMembers(),
                'current_musical' => $group->getMusic()->getName(),
                'presentation' => $group->getPresentation(),
            ];
        }

        return $this->json($data);
    }

    #[Route('/project/{id}', name: 'project_show', methods: ['get'])]
    function show(ManagerRegistry $doctrine, int $id): JsonResponse
    {
        $project = $doctrine->getRepository(Project::class)->find($id);

        if (!$project) {

            return $this->json('Data not found ' . $id, 404);
        }
        $data = [
            'id' => $project->getId(),
            'group_name' => $project->getGroupName(),
            'origin' => $project->getOrigin()->getName(),
            'country' => $project->getCountry()->getName(),
            'start_year' => $project->getStartYear(),
            'separation_year' => $project->getSeparationYear(),
            'founders' => $project->getFounders(),
            'members' => $project->getMembers(),
            'current_musical' => $project->getMusic()->getName(),
            'presentation' => $project->getPresentation(),
        ];

        return $this->json($data);
    }

    /**
     * @Route("/upload-excel", name="xlsx")
     * @param Request $request
     * @throws \Exception
     */
    function xslx(Request $request, PersistenceManagerRegistry $doctrine)
    {
        $file = $request->files->get('file');

        $fileFolder = __DIR__ . '/../../public/uploads/';

        $filePathName = md5(uniqid()) . $file->getClientOriginalName();
        try {
            $file->move($fileFolder, $filePathName);
        } catch (FileException $e) {
            dd($e);
        }

        $spreadsheet = IOFactory::load($fileFolder . $filePathName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $entityManager = $doctrine->getManager();

        foreach ($sheetData as $row) {
            $singer = new Project();
            $singer->setGroupName($row['A']);

            $singer->setStartYear($row['D']);
            $singer->setSeparationYear($row['E']);
            $singer->setFounders($row['F']);
            $singer->setMembers($row['G']);
            $singer->setPresentation($row['I']);
            if ($row['H'] != null) {
                $music = new Music();
                $music->setName($row['H']);
                $entityManager->persist($music);
            }
            if ($row['C'] != null) {
                $country = $entityManager->getRepository(Country::class)->findOneBy(['name' => $row['C']]);
                if (!$country) {
                    $country = new Country();
                    $country->setName($row['C']);
                    $entityManager->persist($country);
                }
                $singer->setCountry($country);
            }
            if ($row['B'] != null) {
                $origin = new Origin();
                $origin->setName($row['B']);
                $entityManager->persist($origin);
            }
            $singer->setMusic($music);
            $singer->setOrigin($origin);

            $entityManager->persist($singer);

            $entityManager->flush();
        }

        return $this->json('singers registered', 200);
    }
}
