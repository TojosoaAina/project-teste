import { Component, PipeTransform } from '@angular/core';
import { FormControl } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { Country } from 'src/app/models/country.model';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/components/modal/modal.component';

@Component({
  selector: 'app-list-song',
  templateUrl: './list-song.component.html',
  styleUrls: ['./list-song.component.scss']
})
export class ListSongComponent {
  country: Country[] = [];
	countries$: Observable<Country[]>;
	filter = new FormControl('', { nonNullable: true });

  constructor(private dataService: DataService, private router: Router, private modalService: NgbModal) {
    this.countries$ = this.filter.valueChanges.pipe(
      startWith(''),
      switchMap(text => this.search(text))
    )
}

navigateTo(id: number) {
  this.router.navigate(['/detailSong/' + id]);
}


search(text: string): Observable<Country[]> {
  return this.dataService.getData().pipe(
    map(countries => {
      if (text.trim() === '') {
        return countries;
      }
      return countries.filter((country: any) => {
        const term = text.toLowerCase();

        return (
          country.group_name.toLowerCase().includes(term)
        );
      });
    })
  );
}
open() {
  const modalRef = this.modalService.open(ModalComponent);
  modalRef.componentInstance.inputValue = '';

  modalRef.result.then((result) => {
    console.log('Closed with:', result);
  }, (reason) => {
    console.log('Dismissed with:', reason);
  });
}

}
